//
//  ViewController.swift
//  AppCenterTest
//
//  Created by Arosha Piyadigama on 7/25/18.
//  Copyright © 2018 AnujAroshA. All rights reserved.
//

import UIKit
import AppCenterAnalytics
import DeviceKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let device = Device()
//
//        if device == .iPhone7 {
//            fatalError()
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func clickMeAction(_ sender: UIButton) {
        MSAnalytics.trackEvent("Click Me Button clicked")
        
        let device = Device()
        
        if device == .iPhone7 {
            fatalError()
        }
    }
}

