//
//  AppCenterTestTests.swift
//  AppCenterTestTests
//
//  Created by Arosha Piyadigama on 7/25/18.
//  Copyright © 2018 AnujAroshA. All rights reserved.
//

import XCTest
@testable import AppCenterTest

class AppCenterTestTests: XCTestCase {
    
    var sut: ViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        sut = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! ViewController
        
        UIApplication.shared.keyWindow?.rootViewController = sut
        
        sut.clickMeAction(UIButton())
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
